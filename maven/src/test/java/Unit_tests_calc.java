import org.testng.annotations.Test;
import org.testng.Assert;

/**
 * Created by Marina on 11.07.2017.
 */
public class Unit_tests_calc {
    @Test
    public void Sum(){
        Assert.assertEquals(7.0,new Calc().math_operation(2,"+",5));
    }

    @Test
    public void Sub(){
        Assert.assertEquals(2.0,new Calc().math_operation(7,"-",5));
    }

    @Test
    public void Multiply(){
        Assert.assertEquals(10.0,new Calc().math_operation(2,"*",5));
    }

    @Test
    public void Divide(){
        Assert.assertEquals(4.0,new Calc().math_operation(20,"/",5));
    }
}
