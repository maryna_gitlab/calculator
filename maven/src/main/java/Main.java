import java.util.*;
import java.util.Scanner;
import java.util.InputMismatchException;



public class Main {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Enter first number");
            double first_number = scanner.nextDouble();
            System.out.println("Enter math operation");
            String math_op = scanner.next();
            System.out.println("Enter second number");
            double second_number = scanner.nextDouble();
            scanner.close();
            System.out.println("Result:" + Calc.math_operation(first_number, math_op, second_number));
        }
        catch(InputMismatchException e){
            System.out.println("Something was wrong.Please enter number and try again.");
        }
    }
}