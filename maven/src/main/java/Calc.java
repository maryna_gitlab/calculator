
/**
 * Created by Marina on 04.07.2017.
 */

import java.util.*;

public class Calc {
    public static double math_operation(double a, String c, double b) {
        double res;
        switch (c) {
            case "+":
                res = a + b;
                return res;
            case "-":
                res = a - b;
                return res;
            case "*":
                res = a * b;
                return res;
            case "/":
                res = a / b;
                return res;
            default:
                System.out.println("Something was wrong. Enable mathematic operations: +-*/");
                break;
        }
        return res = 0;
    }
}
